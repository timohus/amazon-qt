import sys

import time
from PyQt5 import QtCore

from PyQt5.QtCore import QThread
from faker import Faker
from config.db import *

from scraper import Scraper


class Collector(QThread):
    message = QtCore.pyqtSignal(str)
    progress = QtCore.pyqtSignal(int)
    progress_limit = QtCore.pyqtSignal(int)
    products = []
    failed_items = []

    def __init__(self):
        QThread.__init__(self)

    def __del__(self):
        self.wait()

    def add_products(self, products):
        self.products = products

    def run(self):
        try:
            self.get_reviews(self.products)
            if self.failed_items:
                self.message.emit('Waiting for retry...')
                time.sleep(60)
                self.get_reviews(self.failed_items)
            else:
                self.progress.emit(0)
                self.message.emit('All Finished!')
        except:
            print(sys.exc_info())

    def get_reviews(self, asins):
        self.failed_items = []
        self.progress_limit.emit(len(asins))
        self.message.emit('Starting to collect...')
        for index, asin in enumerate(asins):
            self.message.emit('Collecting ' + str(index + 1) + ' of ' + str(len(asins)) + ' (' + asin + ')...')
            try:
                scraper = Scraper(asin)
                reviews = scraper.get_reviews()
                if reviews:
                    for review in reviews:
                        self.save_review(review)
                else:
                    raise Exception('Failed to load data for ' + asin)
            except Exception as e:
                if asin not in self.failed_items:
                    self.failed_items.append(asin)
                print('Exception: ', e)
                print(asin + ' failed. ' + str(len(self.failed_items)) + ' failed items so far')
            time.sleep(1)
            self.progress.emit(index + 1)
        if self.failed_items:
            self.get_reviews(self.failed_items)
            # todo
            # When It can't retrieve item forever, there appears an infinite loop and python
            # throws Fatal Python error: Cannot recover from stack overflow.
            # So There should be some type of attempts limiter

    @staticmethod
    def save_review(review):
        s = ScopedSession()
        s.add(review)
        s.commit()
        s.close()

    def get_user_name(self, id):
        ur = self.amzn.user_reviews(Id=id)
        ur.brief_reviews
        return ur.name

    @staticmethod
    def prepare_author(author):
        if author != 'Amazon Customer':
            return author
        else:
            fake = Faker()
            return fake.name()
