import scrapy
from scrapy.crawler import CrawlerRunner, CrawlerProcess, Crawler
from scrapy.selector import Selector
from scrapy.utils.project import get_project_settings
from twisted.internet import reactor


class Pages(scrapy.Item):
    number = scrapy.Field()


class NumberOfPages(scrapy.Spider):
    name = "quotes"
    start_urls = []

    def __init__(self, url):
        self.start_urls = [url]

    def parse(self, response):
        # n = response.xpath('span.totalReviewCount').extract_first()
        n = Selector(response).xpath('//*[@id="cm_cr-product_info"]/div/div[1]/div[2]/div/div/div[2]/div/span')
        item = Pages()
        item['number'] = n
        print(item)
        return item

if __name__ == 'main':
    settings = get_project_settings()
    process = Crawler(settings)
    process.crawl(NumberOfPages)
    process.start() # the script will block here until the crawling is finished
