import os
import sqlite3

from sqlalchemy import MetaData
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker

db_path = os.path.join(os.path.dirname(__file__), '../db/main.db')
db_uri = 'sqlite:///{}'.format(db_path)


def db():
    conn = sqlite3.connect(db_path)
    return conn.cursor()


def engine():
    return create_engine(db_uri, echo=False)


def db_session():
    Session = sessionmaker(bind=engine())
    return Session()


def db_metadata():
    return MetaData(bind=engine())


Base = declarative_base(bind=engine())
Session = sessionmaker(bind=engine())
ScopedSession = scoped_session(sessionmaker(bind=engine()))

