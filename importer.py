import math
import sys
import time

import shopify
from PyQt5 import QtCore
from PyQt5.QtCore import QThread
from grab import Grab

from config.apis import shopify_shop_url, amazon, AMAZON_ASSOC_TAG
from config.db import ScopedSession
from models.product import Product


class CsvImporter(QThread):
    exiting = False
    message = QtCore.pyqtSignal(str)
    progress = QtCore.pyqtSignal(int)
    progress_limit = QtCore.pyqtSignal(int)
    update_totals = QtCore.pyqtSignal(bool)
    file = ''
    csv_rows = []
    products_handles = {}
    products_skus = {}
    products = []
    update = False
    attempt = 1
    max_attempts = 5
    failed_items = []

    def __init__(self):
        QThread.__init__(self)

    def __del__(self):
        self.wait()

    def add_products(self, products):
        self.products = products

    def get_item_data(self, item, attempt=1):
        data = {'reviews': 0, 'pages': 0, 'reviews_url': ''}
        if item.reviews[0]:
            time.sleep(1)
            g = Grab(timeout=6000)
            url = item.offer_url.replace('http://', 'https://').replace('?tag='+AMAZON_ASSOC_TAG, '')
            try:
                g.go(url)
                if g.response.code == 200 and g.response.url == url:
                    number_of_reviews = int(
                        g.css_text('#acrCustomerReviewText').replace(' customer review', '').replace('s', ''))
                    data['reviews'] = number_of_reviews
                    data['pages'] = int(math.ceil(number_of_reviews / 10))
                    data['reviews_url'] = g.css('.product-reviews-link').get('href')
                    return data
                else:
                    attempt += 1
                    self.retry(item, attempt)
            except:
                attempt += 1
                self.retry(item, attempt)
        return data

    def retry(self, item, attempt):
        self.message.emit('Retrying ' + item.asin + ' (' + str(attempt) + ')')
        if attempt < self.max_attempts:
            time.sleep(1 * attempt)
            self.get_item_data(item, attempt)
        else:
            self.message.emit('Exceeded max retry attempts.')
            self.failed_items.append({'asin':item.asin})
            time.sleep(5)

    def products_lookup(self, strings):
        self.message.emit('Starting...')
        self.progress_limit.emit(len(self.products))
        i = 1
        for asins in strings:
            products_list = amazon.lookup(ItemId=asins)
            for item in products_list:
                self.message.emit('Retrieving data for: ' + item.asin)
                product_url = item._safe_get_element_text('DetailPageURL').replace('http://', 'https://')
                reviews_url = product_url.split('/dp/', 1)[0] + '/product-reviews/' + item.asin + '/'
                parent_asin = str(item.parent_asin) if item.parent_asin else ''
                self.update_product(
                    asin=item.asin,
                    parent_asin=parent_asin,
                    product_url=product_url,
                    title=item.title,
                    image=item.medium_image_url if item.medium_image_url else item.large_image_url,
                    has_reviews=item.reviews[0],
                    reviews_url=reviews_url
                )
                self.progress.emit(i)
                i += 1
                self.update_totals.emit(True)
            time.sleep(1)

        if self.failed_items:
            self.message.emit('Retrying for ' + str(len(self.failed_items)) + ' failed items...')
            time.sleep(5)
            new_strings = self.create_strings_for_request(self.failed_items)
            self.products_lookup(new_strings)
        else:
            self.exiting = True
            self.progress.emit(0)
            self.message.emit('All Finished!')

    @staticmethod
    def create_strings_for_request(asins, package_size=10):
        return [','.join(asins[x:x + package_size]) for x in range(0, len(asins), package_size)]

    def run(self):
        if not self.exiting:
            try:
                if self.products:
                    strings = self.create_strings_for_request(self.products)
                    self.products_lookup(strings)
                else:
                    self.message.emit('Nothing to look for.')
            except:
                print(sys.exc_info())

    def update_product(self, **kwargs):
        s = ScopedSession()
        product = Product(
            asin=kwargs['asin'],
            parent_asin=kwargs['parent_asin'],
            title=kwargs['title'],
            product_url=kwargs['product_url'],
            image=kwargs['image'],
            reviews_url=kwargs['reviews_url'],
            has_reviews=kwargs['has_reviews']
        )
        product_in_db = s.query(Product).filter_by(asin=kwargs['asin']).one_or_none()
        if not product_in_db:
            s.add(product)
            s.commit()
        else:
            s.query(Product).filter_by(asin=kwargs['asin']).update(kwargs)
            s.commit()

    @staticmethod
    def get_number_of_pages(reviews):
        return math.ceil(reviews / 10)

    def product_handle(self, asin):
        return self.products_handles.get(asin, 'none')

    def product_sku(self, asin):
        return self.products_skus.get(asin, 'none')

    def get_all_products_from_shopify(self):
        limit = 250
        shopify.ShopifyResource.set_site(shopify_shop_url)
        count = shopify.Product.count()
        pages = math.ceil(count/limit)
        arr = {}
        self.progress_limit.emit(pages)
        for i in range(1,pages+1):
            products = shopify.Product.find(limit=limit, page=i)
            for product in products:
                for variant in product.variants:
                    arr[variant.sku] = product.handle
            self.message.emit('Collecting handles ...')
            self.progress.emit(i)
        self.progress.emit(0)
        self.message.emit('Finished collecting handles. Please wait ...')
        return arr
