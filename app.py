import csv
import sys

from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from collector import Collector
from config.db import Session, ScopedSession
from importer import CsvImporter
from models.product import Product
from windows.export import ExportWindow
from windows.main import Main


class App(QMainWindow):
    collector = Collector()
    csv_importer = CsvImporter()
    exclude_same_parent_asins = True
    export_window = None

    def __init__(self):
        super().__init__()
        self.init_ui()

    def init_ui(self):
        grid = QGridLayout()
        grid.setSpacing(10)

        exitAction = QAction(QIcon('./icons/exit.png'), '&Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(qApp.quit)

        collect_action = QAction(QIcon('./icons/collect.png'), '&Collect Reviews', self)
        collect_action.triggered.connect(self.collect_all_reviews)

        import_action = QAction(QIcon('./icons/import.png'), '&Import Products', self)
        import_action.triggered.connect(self.import_products)

        update_products_action = QAction(QIcon('./icons/update.png'), '&Update Products', self)
        update_products_action.triggered.connect(self.update_products)

        settings_action = QAction(QIcon('./icons/settings.png'), '&Settings', self)
        settings_action.triggered.connect(self.open_settings)

        export_action = QAction(QIcon('./icons/export.png'), '&Export', self)
        export_action.triggered.connect(self.export_reviews)

        self.stop_action = QAction(QIcon('./icons/exit.png'), '&Stop', self)
        self.stop_action.triggered.connect(self.stop_all)
        self.stop_action.setEnabled(False)

        menu = self.menuBar()
        file_menu = menu.addMenu('&File')
        products_menu = menu.addMenu('&Products')
        reviews_menu = menu.addMenu('&Reviews')
        settings_menu = menu.addMenu('&Settings')
        help_menu = menu.addMenu('&Help')
        file_menu.addAction(exitAction)

        self.toolbar = self.addToolBar('Exit')
        self.toolbar.addAction(import_action)
        self.toolbar.addAction(update_products_action)
        self.toolbar.addAction(collect_action)
        self.toolbar.addAction(export_action)
        self.toolbar.addAction(settings_action)
        self.toolbar.addAction(self.stop_action)

        self.progress = QProgressBar(self)
        #self.progress.setStyleSheet('height: 10px')

        self.message = QLabel("Click the Button to view Error Detail.")

        self.statusBar().setStyleSheet("QStatusBar QLabel { padding: 5px; } QStatusBar QProgressBar { margin-bottom: 5px; margin-right: 3px;}")
        self.statusBar().addWidget(self.message, 1)
        self.statusBar().addWidget(self.progress, 2)

        self.main_window = Main()
        self.setCentralWidget(self.main_window)

        self.setGeometry(120, 120, 900, 600)
        self.setWindowTitle('Amazon Reviews')
        self.setWindowIcon(QIcon('icon-sm.png'))

        self.horizontalGroupBox = QGroupBox("Grid")
        self.horizontalGroupBox.setLayout(grid)

        windowLayout = QVBoxLayout()
        windowLayout.addWidget(self.horizontalGroupBox)
        self.setLayout(windowLayout)

        self.show()

    def export_reviews(self):
        self.export_window = ExportWindow()
        self.export_window.setGeometry(140, 140, 400, 200)
        self.export_window.show()

    def open_settings(self):
        print('Opening Settings')

    def update_products(self, csv_file=False):
        if csv_file:
            products = self.read_file(csv_file)
            if len(products) > 1:
                asins = [row['asin'] for row in products]
        else:
            s = ScopedSession()
            products = s.query(Product).all()
            asins = [row.asin for row in products]
        if asins:
            self.csv_importer.exiting = False
            self.csv_importer.message.connect(self.update_message)
            self.csv_importer.progress_limit.connect(self.set_progress_range)
            self.csv_importer.progress.connect(self.update_progress)
            self.csv_importer.update_totals.connect(self.update_totals)
            self.csv_importer.add_products(asins)
            self.csv_importer.start()
            self.stop_action.setEnabled(True)

    def stop_all(self):
        self.csv_importer.exiting = True
        self.message.setText('Stopped.')
        self.update_progress(0)

    def read_file(self, file):
        items = []
        try:
            with open(file, 'r', newline='') as csv_file:
                rows = csv.DictReader(csv_file, dialect='excel-tab')
                for row in rows:
                    items.append(row)
        except:
            self.update_message("Can't open file")
        return items

    def import_products(self):
        csv_file = QFileDialog.getOpenFileName(self, 'Select SCV File', 'C:\Tim\REVIEWS', "Csv files (*.csv *.txt);;All files (*.*)")
        self.update_products(csv_file[0])

    def collect_all_reviews(self):
        s = ScopedSession()
        products = s.query(Product).all()
        # if self.exclude_same_parent_asins and asin.parent_asin != '':
        #     products = map()
        # asins = [row.asin for row in filter(lambda x: self.filter_asins(x), products)]
        asins = self.filter_products(products)
        print(len(asins))
        if asins:
            self.collector.progress_limit.connect(self.set_progress_range)
            self.collector.message.connect(self.update_message)
            self.collector.progress.connect(self.update_progress)
            self.collector.add_products(asins)
            self.collector.start()

    def filter_products(self, products):
        # todo
        # Better algorythm for filtering products when parent asin is the same.
        # To many db request when finding parents first child
        arr = []
        for product in products:
            if product.has_reviews:
                if self.exclude_same_parent_asins and product.parent_asin != '':
                    asin = self.find_parents_first_child(product.parent_asin)
                    if asin not in arr:
                        arr.append(asin)
                else:
                    arr.append(product.asin)
        return arr

    def find_parents_first_child(self, asin):
        s = ScopedSession()
        product = s.query(Product).filter(Product.parent_asin == asin).first()
        return product.asin

    def filter_asins(self, asin):
        if asin.has_reviews:
            return True
        return False

    def update_message(self, message):
        self.message.setText(message)

    def update_progress(self, n):
        self.progress.setValue(n)

    def update_totals(self, update=False):
        if update:
            # self.main_window.populate_products_table()
            self.main_window.update_totals()

    def set_progress_range(self, j):
        self.progress.setRange(0, j)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
