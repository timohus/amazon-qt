#Amazon Reviews Collector#

##Problem Definition##
* Scraping reviews from Amazon
* Exporting reviews in several covenient ways
* Running updates on new reviews

##Requirements##
* User creates lists of products
* Multiple lists may exist. Lists may share same products. A list is like grouping tool
* Lists are being stored localy
* Adding new products to the list may be done via csv/xls import or by single product
* First a list must be created. Then it's being populated with products