import random
import time
from grab import Grab


class Spider:
    url = ''
    max_attempts = 5
    html = ''

    def __init__(self, url=''):
        self.url = url

    def crawl(self):
        attempt = 1
        try:
            self.make_attempt(attempt)
            if self.html:
                return self.html
        except:
            print('Item Failed.')

    def make_attempt(self, attempt=1):
        if attempt <= self.max_attempts:
            time.sleep(1 * attempt)
            g = Grab(timeout=6000)
            g.go(self.url)
            if g.response.code == 200 and g.response.url == self.url:
                self.html = g.response.body
            else:
                self.make_attempt(attempt + 1)
        else:
            raise Exception('Exceeded max attempts')

    def load_user_agents(self):
        uas = []
        with open('//helpers/user_agents.txt', 'rb') as uaf:
            for ua in uaf.readlines():
                if ua:
                    uas.append(ua.strip()[1:-1 - 1])
        random.shuffle(uas)
        return uas

if __name__ == 'main':
    Spider()


# todo
# ip address rotation