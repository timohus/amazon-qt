from sqlalchemy import *
from config.db import *


class Product(Base):
    __tablename__ = 'products'

    id = Column(Integer, Sequence('product_seq'), primary_key=True)
    asin = Column(String)
    parent_asin = Column(String)
    title = Column(String)
    product_url = Column(String)
    reviews_url = Column(String)
    image = Column(String)
    has_reviews = Column(Boolean)

    # def __init__(self):
    #     Base.metadata.create_all()

    def __repr__(self):
        return "<Product(asin='%s', parent_asin='%s', sku='%s', handle='%s', title='%s', url='%s', reviews_url='%s', reviews='%s', pages='%s')>" % (
            self.asin, self.parent_asin, self.sku, self.handle, self.title, self.url, self.reviews_url, self.reviews, self.pages)


if __name__ == "__main__":
    Product()
