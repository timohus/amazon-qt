import datetime
import sqlite3

from pymongo import *


class Settings:
    def __init__(self):
        print('Hi!')
        # client = MongoClient('mongodb://localhost:27017/')
        # db = client.test_database
        # post = {"author": "Mike",
        #         "text": "My first blog post!",
        #         "tags": ["mongodb", "python", "pymongo"],
        #         "date": datetime.datetime.utcnow()}
        # posts = db.posts
        # post_id = posts.insert_one(post).inserted_id
        # print('Here\'s a post id')
        # print(post_id)
        conn = sqlite3.connect('settings.db')
        c = conn.cursor()
        c.execute('''CREATE TABLE stocks
                     (date text, trans text, symbol text, qty real, price real)''')
        c.execute("INSERT INTO stocks VALUES ('2006-01-05','BUY','RHAT',100,35.14)")
        conn.commit()
        conn.close()

if __name__ == "__main__":
    print('Hello')
    Settings()
