from sqlalchemy import *
from config.db import *


class Review(Base):
    __tablename__ = 'reviews'

    id = Column(Integer, Sequence('review_seq'), primary_key=True)
    asin = Column(String)
    title = Column(String)
    author = Column(String)
    rating = Column(Integer)
    body = Column(String)
    created_at = Column(String)

    # def __init__(self):
    #     Base.metadata.create_all()

    def __repr__(self):
        return "<Review(asin='%s', title='%s', author='%s', rating='%s', body='%s', created_at='%s')>" % (
            self.asin, self.title, self.author, self.rating, self.body, self.created_at)


if __name__ == "__main__":
    Review()

