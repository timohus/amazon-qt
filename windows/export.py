import csv

import math

import re
import shopify
import sys
from PyQt5.QtWidgets import *

from config.apis import shopify_shop_url
from config.db import ScopedSession
from models.product import Product
from models.review import Review


class ExportWindow(QWidget):
    rating = [2, 3, 4, 5]
    rating_checkboxes = []
    strip_html = True

    def __init__(self):
        super().__init__()
        self.init_ui()

    def init_ui(self):
        fbox = QFormLayout()
        fbox.setVerticalSpacing(10)

        hbox = QHBoxLayout()
        for i in range(1, 6):
            b = QCheckBox(str(i))
            if i in self.rating:
                b.setChecked(True)
            b.stateChanged.connect(self.set_rating)
            self.rating_checkboxes.append(b)
            hbox.addWidget(b)
        hbox.addStretch()

        self.b_strip_html = QCheckBox('Strip Html Tags')
        self.b_strip_html.setChecked(self.strip_html)
        self.b_strip_html.stateChanged.connect(self.set_strip_html)

        self.b_start_export = QPushButton('Export')
        self.b_start_export.clicked.connect(self.start_export)

        buttons_row = QHBoxLayout()
        buttons_row.addWidget(self.b_start_export)
        buttons_row.addWidget(QPushButton("Cancel"))

        fbox.addRow(QLabel('Include Ratings: '), hbox)
        fbox.addRow(self.b_strip_html)
        fbox.addRow(QLabel(''), buttons_row)

        self.setLayout(fbox)
        self.setWindowTitle('Export Reviews')
        self.show()

    def set_rating(self):
        arr = []
        for i, b in enumerate(self.rating_checkboxes):
            if b.isChecked():
                arr.append(i+1)
        self.rating = arr

    def set_strip_html(self):
        if self.b_strip_html.isChecked():
            self.strip_html = True
        else:
            self.strip_html = False

    def start_export(self):

        csv_file = QFileDialog.getOpenFileName(self, 'Select SCV File', 'C:\Tim\REVIEWS',
                                               "Csv files (*.csv *.txt);;All files (*.*)")
        print(csv_file[0])
        csv_rows = self.read_file(csv_file[0])
        print('Total csv_rows: '+ str(len(csv_rows)))
        # s = ScopedSession()
        # products = s.query(Product).all()
        # s.close()
        products = self.get_products()
        print(len(products))
        print('Grabing Products form Shopify')
        shopify_products = self.get_all_products_from_shopify()
        print('Total shopify products: ' + str(len(shopify_products)))
        print('Getting reviews')
        reviews_list = self.get_reviews_list()
        print('Preparing data for export...')
        arr = []
        for product in products:
            sku = csv_rows.get(product.asin, None)
            if sku:
                handle = shopify_products.get(sku, None)
                if handle:
                    print(handle)
            #         # arr.append({'asin': product.asin, 'sku': sku, 'title': product.title, 'handle': handle})
                    if product.parent_asin:
                        s2 = ScopedSession()
                        p = s2.query(Product).filter(Product.parent_asin == product.parent_asin).first()
                        s2.close()
                        if product.asin == p.asin:
                            reviews = reviews_list.get(p.asin, [])
                    else:
                        reviews = reviews_list.get(product.asin, [])

                    for review in reviews:
                        if int(review.rating) in self.rating:
                            body = re.sub("<.*?>", "", review.body) if self.strip_html else review.body
                            arr.append({'asin': product.asin, 'sku': sku, 'product_handle': handle, 'rating': review.rating, 'title': review.title, 'author': review.author, 'email': 'amazonreview@amazon.com', 'body': body, 'created_at': review.created_at})
        print('Total reviews: ' + str(len(arr)))
        name = QFileDialog.getSaveFileName(self, 'Save File')
        file = open(name[0], 'w')
        writer = csv.writer(file, lineterminator='\n')
        writer.writerow(['asin', 'sku', 'product_handle', 'rating', 'title', 'author', 'email', 'body', 'created_at'])
        # writer.writerow(['asin', 'sku', 'title', 'handle'])
        for item in arr:
            try:
                # writer.writerow([item['asin'], item['sku'], item['title'], item['handle']])

                writer.writerow([item['asin'], item['sku'], item['product_handle'], str(item['rating']), self.remove_emoji(item['title']), self.remove_emoji(item['author']), item['email'], self.remove_emoji(item['body']), item['created_at']])
            except:
                print(sys.exc_info())
        file.close()

    @staticmethod
    def get_reviews_list():
        arr = {}
        s = ScopedSession()
        groups = s.query(Review).group_by(Review.asin).all()
        for group in groups:
            reviews = s.query(Review).filter(Review.asin == group.asin).all()
            arr[group.asin] = reviews
        s.close()
        return arr

    @staticmethod
    def get_products():
        arr = []
        s = ScopedSession()
        products = s.query(Product).group_by(Product.parent_asin).all()
        s.close()
        for product in products:
            arr.append(product)
        return arr

    @staticmethod
    def prepare_text(text):
        return str(text.encode('utf-8')).replace("b'", '').replace('b"', '')[:-1]

    # def remove_emoji(self, text):
    #     try:
    #         # Wide UCS-4 build
    #         myre = re.compile(u'['
    #                           u'\U0001F300-\U0001F64F'
    #                           u'\U0001F680-\U0001F6FF'
    #                           u'\u2600-\u26FF\u2700-\u27BF]+',
    #                           re.UNICODE)
    #     except re.error:
    #         # Narrow UCS-2 build
    #         myre = re.compile(u'('
    #                           u'\ud83c[\udf00-\udfff]|'
    #                           u'\ud83d[\udc00-\ude4f\ude80-\udeff]|'
    #                           u'[\u2600-\u26FF\u2700-\u27BF])+',
    #                           re.UNICODE)
    #     string = myre.sub('', text)
    #     return self.prepare_text(text)

    @staticmethod
    def remove_emoji(data):
        try:
            # UCS-4
            patt = re.compile(u'([\U00002600-\U000027BF])|([\U0001f300-\U0001f64F])|([\U0001f680-\U0001f6FF])')
        except re.error:
            # UCS-2
            patt = re.compile(
                u'([\u2600-\u27BF])|([\uD83C][\uDF00-\uDFFF])|([\uD83D][\uDC00-\uDE4F])|([\uD83D][\uDE80-\uDEFF])')
        return patt.sub('', data)

    def find_sku(self, asin, rows):
        for row in rows:
            if row['asin'] == asin:
                return row['sku']
        return None

    def get_all_products_from_shopify(self):
        limit = 250
        shopify.ShopifyResource.set_site(shopify_shop_url)
        count = shopify.Product.count()
        pages = math.ceil(count/limit)
        arr = {}
        for i in range(1,pages+1):
            products = shopify.Product.find(limit=limit, page=i)
            for product in products:
                for variant in product.variants:
                    arr[variant.sku] = product.handle
        return arr

    def read_file(self, file):
        items = {}
        try:
            with open(file, 'r', newline='') as csv_file:
                rows = csv.DictReader(csv_file, dialect='excel-tab')
                for row in rows:
                    items[row['asin']] = row['sku']
        except:
            self.update_message("Can't open file")
        return items
