from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import *

from config.db import Session, ScopedSession
from models.product import Product


class Main(QWidget):
    def __init__(self):
        super().__init__()
        self.init_ui()

    def init_ui(self):
        hbox = QHBoxLayout(self)

        self.prodcuts_table = QTreeWidget(self)
        header = QTreeWidgetItem(['ASIN', 'Title', 'Has Reviews'])
        self.prodcuts_table.setHeaderItem(header)
        self.populate_products_table()
        self.prodcuts_table.itemClicked.connect(self.show_item_info)
        self.prodcuts_table.setAutoScroll(False)

        self.output = QTextEdit(self)

        topright = QWidget(self)
        topright.setFixedWidth(240)

        grid = QGridLayout()
        grid.setSpacing(5)
        grid.setRowStretch(2, 1)
        topright.setLayout(grid)

        self.totals_label = QLabel("Products: ")
        self.totals = QLabel("0")

        self.with_reviews_label = QLabel("Have reviews:")
        self.with_reviews = QLabel("0")

        grid.addWidget(self.totals_label, 0, 0)
        grid.addWidget(self.totals, 0, 1)
        grid.addWidget(self.with_reviews_label, 1, 0)
        grid.addWidget(self.with_reviews, 1, 1)

        splitter1 = QSplitter(Qt.Horizontal)
        splitter1.addWidget(self.prodcuts_table)
        splitter1.addWidget(topright)

        splitter2 = QSplitter(Qt.Vertical)
        splitter2.addWidget(splitter1)
        splitter2.addWidget(self.output)
        splitter2.setSizes([400, 60])

        hbox.addWidget(splitter2)

        self.update_totals()

        self.setLayout(hbox)
        self.show()

    def populate_products_table(self):
        s = ScopedSession()
        products = s.query(Product).all()
        for item in products:
            has_url = 'yes' if item.reviews_url else 'no'
            QTreeWidgetItem(self.prodcuts_table, [item.asin, item.title, str(item.has_reviews)])
        s.close()

    def show_item_info(self, item):
        pass
        #reply = QMessageBox.question(self, 'Message', "Are you sure to quit?", QMessageBox.Yes | QMessageBox.No, QMessageBox.No)

    def update_totals(self):
        s = ScopedSession()
        self.totals.setText(str(s.query(Product).count()))
        self.with_reviews.setText(str(s.query(Product).filter(Product.has_reviews != 0).count()))
        s.close()


if __name__ == '__main__':
    Main()