import lxml
from bs4 import BeautifulSoup
from faker import Faker

from config.db import ScopedSession
from models.product import Product
from models.review import Review
from spider import Spider


class Scraper:
    asin = ''
    reviews_url = ''
    reviews = []

    def __init__(self, asin):
        s = ScopedSession()
        product = s.query(Product).filter(Product.asin == asin).one()
        s.close()
        self.asin = asin
        self.reviews_url = product.reviews_url

    def get_reviews(self):
        pages = self.get_reviews_pages()
        if pages:
            for page in pages:
                self.get_reviews_by_page(page)
        else:
            raise Exception('No pages')
        return self.reviews

    def get_reviews_by_page(self, url):
        spider = Spider(url)
        html = spider.crawl()

        if html:
            soup = BeautifulSoup(html, 'html.parser')
            blocks = soup.find_all('div', {'class': 'review'})
            for block in blocks:
                review = Review(
                    asin=self.asin,
                    title=block.select_one('a.review-title').text,
                    author=self.prepare_author(block.select_one('a.author').text),
                    rating=int(float(block.select_one('i.review-rating > span').get_text().replace(' out of 5 stars', ''))),
                    body=block.select_one('span.review-text').text,
                    created_at=block.select_one('span.review-date').text.replace('on ', ''),
                )
                self.reviews.append(review)
        else:
            raise Exception('Failed to parse reviews page')

    def get_reviews_pages(self):
        arr = []
        spider = Spider(self.reviews_url)
        html = spider.crawl()

        if html:
            tree = lxml.html.fromstring(html)
            n = int(tree.xpath('//*[@id="cm_cr-product_info"]/div/div[1]/div[2]/div/div/div[2]/div/span/text()')[0])
            if n > 0:
                for i in range(1, n+1):
                    page_url = self.reviews_url + '?sortBy=recent&pageNumber=' + str(i)
                    arr.append(page_url)
        else:
            print('failed to get pages')
            raise Exception('Failed to get pages')
        return arr

    @staticmethod
    def prepare_author(author):
        if author != 'Amazon Customer':
            return author
        else:
            fake = Faker()
            return fake.name()
